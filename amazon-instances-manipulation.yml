---
- hosts: localhost
  gather_facts: False
  tasks:
    - name: Provisioning instances
      block:
      - name: Create a new ec2 key pair, returns generated private key
        ec2_key:
          name: "{{ private_key_name }}"
          region: us-east-1
        register: ec2_key_result
      - name: Save private key
        copy:
            content: "{{ ec2_key_result.key.private_key }}"
            dest: "{{ private_key_path }}"
            mode: 0600
        when: ec2_key_result.changed
      - name: Create security group for cassandra nodes
        ec2_group:
          name: cassandra-group
          description: allow ssh outside and all traffic inside
          region: us-east-1
          state: present
          rules:
            - proto: tcp
              from_port: 22
              to_port: 22
              cidr_ip: 0.0.0.0/0
            - proto: all
              cidr_ip: 10.0.0.0/8
      - name: Provision a set of instances
        ec2:
           key_name: "{{ private_key_name }}"
           group: cassandra-group
           instance_type: t2.micro
           region: us-east-1
           image: "ami-04b9e92b5572fa0d1"
           wait: true
           exact_count: 1
           count_tag:
              name: cassandra-instances
           instance_tags:
              name: cassandra-instances
        register: ec2
      - name: Add all instance public IPs to host group
        add_host:
          hostname: "{{ item.public_ip }}"
          groups: ec2hosts
        loop: "{{ ec2.instances }}"
      - name: Wait for SSH to come up
        delegate_to: "{{ item.public_dns_name }}"
        wait_for_connection:
          delay: 10
          timeout: 120
        loop: "{{ ec2.instances }}"
      when: instance_state == "present"
    - name: Get instances and change state to "{{ instance_state }}"
      block:
      - name: Get instaces by tags
        ec2_instance_info:
          region: us-east-1
          filters:
            "tag:name": cassandra-instances
            instance-state-name: "{{ (instance_state == 'stopped') | ternary(['running'], ['running', 'stopped', 'stopping']) }}"
        register: selected_instances
      - name: Change state of instances to "{{ instance_state }}"
        ec2:
          wait: True
          region: us-east-1
          state: "{{ instance_state }}"
          instance_ids: "{{ item.instance_id }}"
        loop: "{{ selected_instances.instances }}"
      when: instance_state != "present"
